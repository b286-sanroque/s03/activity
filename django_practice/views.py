# from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from .models import GroceryItem

from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict


def index(request):
	groceryitem_list = GroceryItem.objects.all()

	context = {
		'groceryitem_list' : groceryitem_list,
		"user" : request.user
	}

	return render(request, "django_practice/index.html", context)
	# return HttpResponse("WELCOME!!")

def groceryitem(request, groceryitem_id):
	groceryitem = model_to_dict(GroceryItem.objects.get(pk=groceryitem_id))
	# response = "You are viewing the details of %s"

	# return HttpResponse(response %groceryitem_id)
	return render(request, "django_practice/groceryitem.html", groceryitem)

def register(request):
	users = User.objects.all()
	is_user_registered = False
	context = {
		"is_user_registered" : is_user_registered
	}

	# check if username already exist
	for indiv_user in users:
		if indiv_user.username == "ayka":
			is_user_registered == True
			break;
	if is_user_registered == False:
		user = User() #empty object
		user.username = "ayka1"
		user.first_name = "Aira"
		user.last_name = "SR"
		user.email = "airasr@mail.com"
		# The 'set_password' is used to ensure that the passord is hashed using Django's authentication framework
		user.set_password("ayka123")
		user.is_staff = False
		user.is_active = True
		user.save()

		context = {
			"first_name" : user.first_name,
			"last_name" : user.last_name,
		}

	return render(request, "django_practice/register.html", context)

def change_password(request):
	is_user_authenticated = False

	user = authenticate(username="ayka1", password="ayka123")
	print(user)
	if user is not None:
		authenticated_user = User.objects.get(username='ayka1') # this is where we get the username "johndoe"
		authenticated_user.set_password("ayka06") # this is where we change password
		authenticated_user.save()
		is_user_authenticated = True
	context = {
		"is_user_authenticated": is_user_authenticated
	}

	return render(request, "django_practice/change_password.html", context)	

def login_view(request):
	username = "ayka1"
	password = "ayka06"
	user = authenticate(username=username, password=password)
	context = {
		"is_user_authenticated" : False
	}
	print(user)
	if user is not None:
		login(request, user)
		return redirect("index")
	else:
		return render(request, "django_practice/login.html", context)

def logout_view(request):
	logout(request)
	return redirect("index")